package com.ipfinder.javid.ipfinder;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ipfinder.javid.ipfinder.YahooModel.Query;
import com.ipfinder.javid.ipfinder.YahooModel.YahooModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Context context = MainActivity.this;
    TextView result;
    EditText cityName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e("onCreate: ", "6512465454598");
        findViewById(R.id.btn).setOnClickListener(this);
        result = findViewById(R.id.result);
        cityName = findViewById(R.id.cityName);

    }

    private void parser(String lat , String lon) {
        String query = "select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text=\"("+lat+","+lon+")\")\n";
        ConnectYahoo.getClient().create(WebServises.class).getWeather(query).enqueue(new Callback<YahooModel>() {
            @Override
            public void onResponse(Call<YahooModel> call, Response<YahooModel> response) {
                YahooModel model = response.body();
                result.setText(model.getQuery().getResults().getChannel().getItem().getTitle());
            }

            @Override
            public void onFailure(Call<YahooModel> call, Throwable t) {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn) {
        }
    }
}