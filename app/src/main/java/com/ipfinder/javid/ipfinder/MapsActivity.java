package com.ipfinder.javid.ipfinder;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ipfinder.javid.ipfinder.YahooModel.YahooModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ImageView marker;
    public double lat;
    public double lon;
    Context mContext = MapsActivity.this;
//    public YahooModel model =new YahooModel();
    public ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        list = findViewById(R.id.list);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                lat = mMap.getCameraPosition().target.latitude;
                lon = mMap.getCameraPosition().target.longitude;
                parser( lat ,  lon);
            }
        });


//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
    private void parser(double lat , double lon) {
        String query = "select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text=\"("+lat+","+lon+")\")\n";
        ConnectYahoo.getClient().create(WebServises.class).getWeather(query).enqueue(new Callback<YahooModel>() {
            @Override
            public void onResponse(Call<YahooModel> call, Response<YahooModel> response) {
                YahooModel model = response.body();
                 try{
               AdapterListView adapter = new AdapterListView(mContext,model.getQuery().getResults().getChannel().getLocation().getCity()
                       ,model.getQuery().getResults().getChannel().getItem().getForecast());
               list.setAdapter(adapter);
                 }catch (Exception e){
                Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                 }
            }

            @Override
            public void onFailure(Call<YahooModel> call, Throwable t) {
                Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
