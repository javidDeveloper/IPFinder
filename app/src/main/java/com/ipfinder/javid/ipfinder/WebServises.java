package com.ipfinder.javid.ipfinder;

import android.database.Observable;

import com.ipfinder.javid.ipfinder.YahooModel.YahooModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WebServises {

    //https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(35.7394274%2C51.4167078)%22)&
    // format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
    @GET("yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    Call<YahooModel> getWeather(@Query("q") String q );
}
