package com.ipfinder.javid.ipfinder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ipfinder.javid.ipfinder.YahooModel.Forecast;
import com.ipfinder.javid.ipfinder.YahooModel.YahooModel;

import java.util.List;

public class AdapterListView extends BaseAdapter {
    private Context mContext;
    private List<Forecast> models;
    private String cityVal;
    private TextView cityName, text, low, high;

    public AdapterListView(Context mContext,String cityVal ,List<Forecast> models) {
        this.mContext = mContext;
        this.models = models;
        this.cityVal = cityVal;
    }
    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.forcast_list_item, parent, false);
        cityName = view.findViewById(R.id.cityName);
        text = view.findViewById(R.id.text);
        low = view.findViewById(R.id.low);
        high = view.findViewById(R.id.high);

        cityName.setText(cityVal.toString());
        text.setText(models.get(position).getText());
        low.setText(models.get(position).getLow());
        high.setText(models.get(position).getHigh());
       return view;
    }
}
